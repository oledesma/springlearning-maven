package com.prueba.anotaciones;

public class Pelicula {
    private String titulo;
    private String anyo;
    private String genero;

    public Pelicula(){}

    public Pelicula(String titulo, String anyo, String genero) {
        this.titulo = titulo;
        this.anyo = anyo;
        this.genero = genero;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getAnyo() {
        return anyo;
    }

    public void setAnyo(String anyo) {
        this.anyo = anyo;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }
}
