package com.prueba.anotaciones;

import com.prueba.lifecycleSample.PersonaDAO;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App {
    public static void main(String[] args) {
        //Inicializando el contexto de la aplicación con el beans
        //ClassPathXmlApplicationContext no funciona en una aplicación web y se producirá una excepción.
        ApplicationContext context = new ClassPathXmlApplicationContext("beansAnnotations.xml");

        PeliculaService peliculaService = context.getBean(PeliculaService.class);
        peliculaService.pelisPorGenero("Amor").forEach(System.out::println);

        //se cierra el conexto
        ((ClassPathXmlApplicationContext)context).close();
    }
}
