package com.prueba.anotaciones;

import com.prueba.lifecycleSample.Persona;
import com.prueba.lifecycleSample.PersonaDAO;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class PeliculaDaoImplMemory implements PeliculaDAO {

    private List<Pelicula> peliculas = new ArrayList<>();


    @Override
    public Pelicula findById(int id) {
        return peliculas.get(id);
    }

    @Override
    public Collection<Pelicula> findAll() {
        return peliculas;
    }

    @Override
    public void insert(Pelicula pelicula) {
        peliculas.add(pelicula);
    }

    @Override
    public void edit(Pelicula antigua, Pelicula nueva) {
        peliculas.remove(antigua);
        peliculas.add(nueva);
    }

    @Override
    public void delete(Pelicula pelicula) {
        peliculas.remove(pelicula);
    }

    public void init() throws Exception{
        insert(new Pelicula("La guerra", "1993", "Blabla"));
        insert(new Pelicula("Thor", "2008", "Dios"));
        insert(new Pelicula("Martina", "2005", "Amor"));
    }

    public void destroy() throws Exception {
        System.out.println("Limpiando los datos de la lista");
        peliculas.clear();
    }
}
