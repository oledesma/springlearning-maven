package com.prueba.spring;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.File;

public class App {
    public static void main(String[] args) {

        //Inicializando el contexto de la aplicación con el beans
        //ClassPathXmlApplicationContext no funciona en una aplicación web y se producirá una excepción.
        ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");

        //Ejemplos de beans
        Saludator saludator = null;

        //1. getBean con ID y casting
        //saludator = (Saludator) context.getBean("saludator");

        //2. getBean con ID y tipo
        //saludator = context.getBean("contstructor", Saludator.class);

        //3. getBean con tipo (Si existe otro que utiliza la misma clase, se produce una excepción)
        //saludator = context.getBean(Saludator.class);

        //Imprimiendo saludo
        System.out.println(saludator.saludo());

        //se cierra el conexto
        ((ClassPathXmlApplicationContext)context).close();
    }
}
