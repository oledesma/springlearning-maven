package com.prueba.spring;

public class Saludator {

    private String mensaje;

    public Saludator(String str){
        this.mensaje = str;
    }

    public Saludator() {
    }

    public void setMensaje(String str){
        this.mensaje = str;
    }

    public String saludo(){
        if(mensaje != null){
            return mensaje;
        }else {
            return "No hay mensajes";
        }
    }
}
