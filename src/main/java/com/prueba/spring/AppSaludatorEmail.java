package com.prueba.spring;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class AppSaludatorEmail {
    public static void main(String[] args) {
        //Inicializando el contexto de la aplicación con el beans
        ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");

        //Ejemplos de beans
        Saludator saludator = null;

        saludator = context.getBean("saludatorAutomatico", Saludator.class);

        System.out.println(saludator.saludo() + "\n\n");

        //mensaje de EmailService
        EmailService emailService = null;

        emailService = context.getBean("emailServiceAuto", EmailService.class);

        emailService.enviarEmailSaludo("oledesma2210@gmail.com");

    }
}
