package com.prueba.spring;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Prototype {
    public static void main(String[] args) {

        //Inicializando el contexto de la aplicación con el beans
        ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");

        //mensaje de EmailService
        EmailService emailService = null;

        emailService = context.getBean("emailServicePrototype", EmailService.class);
        emailService.setDestinatarioPorDefecto("oledesma2210@gmail.com");
        emailService.enviarEmailSaludo();


        EmailService emailService2 = null;
        emailService2 = context.getBean("emailServicePrototype", EmailService.class);
        emailService2.enviarEmailSaludo();

        //se cierra el conexto
        ((ClassPathXmlApplicationContext)context).close();
    }
}
