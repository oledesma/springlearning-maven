package com.prueba.spring;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SingleTon {
    public static void main(String[] args) {
        //Inicializando el contexto de la aplicación con el beans
        ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");


        //mensaje de EmailService
        EmailService emailService = null;

        emailService = context.getBean("emailServiceSingleton", EmailService.class);

        EmailService emailService2 = null;

        emailService2 = context.getBean("emailServiceSingleton", EmailService.class);

        System.out.println(emailService);
        System.out.println(emailService2);

        //se cierra el conexto
        ((ClassPathXmlApplicationContext)context).close();
    }
}
