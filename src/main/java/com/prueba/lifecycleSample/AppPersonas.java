package com.prueba.lifecycleSample;

import com.prueba.spring.Saludator;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class AppPersonas {
    public static void main(String[] args) {

        //Inicializando el contexto de la aplicación con el beans
        //ClassPathXmlApplicationContext no funciona en una aplicación web y se producirá una excepción.
        ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");

        PersonaDAO personaDAO = context.getBean("personaDao", PersonaDAO.class);
        PersonaDAO personaDAO2 = context.getBean("personaDaoInit", PersonaDAO.class);

        System.out.println("Con interfaz InitzialiingBeans y DisposableBeans");
        personaDAO.findAll().forEach(System.out::println);


        System.out.println("\nCon método en beans de init-method='init()'");
        personaDAO2.findAll().forEach(System.out::println);

        System.out.println("\nEs solo una prueba, ambos tipos de métodos, deberían por separado");

        //se cierra el conexto
        ((ClassPathXmlApplicationContext)context).close();
    }
}
