package com.prueba.lifecycleSample;

import java.util.ArrayList;
import java.util.List;

public class PersonaDAOImplMemoryBeansMethod implements PersonaDAO{

    List<Persona> personas = new ArrayList<Persona>();
    public Persona findByIndex(int index) {
        return personas.get(index);
    }

    public List<Persona> findAll() {
        return personas;
    }

    public void insert(Persona persona) {
        personas.add(persona);
    }

    public void edit(int index, Persona persona) {
        personas.remove(index);
        personas.add(index, persona);
    }

    public void delete(int index) {
        personas.remove(index);
    }

    public void delete(Persona persona) {
        personas.remove(persona);
    }

    public void init() throws Exception{
        insert(new Persona("Oscar", 20));
        insert(new Persona("Manuel", 25));
        insert(new Persona("Maria", 34));
        insert(new Persona("Montse", 22));
        insert(new Persona("Pablo", 19));
    }

    public void destroy() throws Exception {
        System.out.println("Limpiando los datos de la lista");
        personas.clear();
    }
}
