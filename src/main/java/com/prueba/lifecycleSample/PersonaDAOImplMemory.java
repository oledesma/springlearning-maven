package com.prueba.lifecycleSample;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

import java.util.ArrayList;
import java.util.List;

public class PersonaDAOImplMemory implements PersonaDAO, InitializingBean, DisposableBean {

    List<Persona> personas = new ArrayList<Persona>();
    public Persona findByIndex(int index) {
        return personas.get(index);
    }

    public List<Persona> findAll() {
        return personas;
    }

    public void insert(Persona persona) {
        personas.add(persona);
    }

    public void edit(int index, Persona persona) {
        personas.remove(index);
        personas.add(index, persona);
    }

    public void delete(int index) {
        personas.remove(index);
    }

    public void delete(Persona persona) {
        personas.remove(persona);
    }

    //lo implementa InitializingBean. Se ejecuta cuando se crea el bean.
    @Override
    public void afterPropertiesSet() throws Exception {
        insert(new Persona("Oscar", 20));
        insert(new Persona("Manuel", 25));
        insert(new Persona("Maria", 34));
        insert(new Persona("Montse", 22));
        insert(new Persona("Pablo", 19));
    }

    @Override
    public void destroy() throws Exception {
        System.out.println("Limpiando los datos de la lista");
        personas.clear();
    }
}
